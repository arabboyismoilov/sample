import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;

import static org.mockito.Mockito.*;

public class HelloTest {
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private PrintWriter writer;

    private Hello hello;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        hello = new Hello();
    }

    @Test
    public void testDoGet() throws Exception {
        // Mocking behavior
        when(response.getWriter()).thenReturn(writer);

        // Calling the method to be tested
        hello.doGet(request, response);

        // Verifying that the response was as expected
        verify(response).setContentType("text/html");
        verify(writer).println("<html><body>");
//        verify(writer).println("<h2>Hello, this is a servlet!</h2>");
        verify(writer).println("</body></html>");
    }

}